package com.sber.hw6.dao;

import com.sber.hw6.mapper.BookMapper;
import com.sber.hw6.model.Book;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("singleton")
public class BookDao {
    private final BookMapper bookMapper = new BookMapper();
    private final Connection connection;

    public BookDao(Connection connection) {
        this.connection = connection;
    }

    public Book findById(Integer id) throws SQLException {
        PreparedStatement preparedStatement = this.connection.prepareStatement("select * from books where id = ?");
        preparedStatement.setInt(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        Book book = new Book();
        while (resultSet.next()) {
            book = bookMapper.createBook(resultSet);
        }
        return book;
    }

    public Book findByTitle(String title) throws SQLException {
        PreparedStatement preparedStatement = this.connection.prepareStatement("select * from books where title like ?");
        preparedStatement.setString(1, title);
        ResultSet resultSet = preparedStatement.executeQuery();
        Book book = new Book();
        while (resultSet.next()) {
            book = bookMapper.createBook(resultSet);
        }
        return book;
    }
}