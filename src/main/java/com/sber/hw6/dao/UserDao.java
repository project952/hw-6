package com.sber.hw6.dao;

import com.sber.hw6.model.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class UserDao {
    private final Connection connection;

    public UserDao(Connection connection) {
        this.connection = connection;
    }

    public void addUser(User user) throws SQLException {
        PreparedStatement preparedStatement = this.connection.prepareStatement("insert into users (surname,first_name,date_of_birth,telephone,email,books_borrowed) values (?,?,?,?,?,?)");
        preparedStatement.setString(1, user.getSurname());
        preparedStatement.setString(2, user.getFirstName());
        preparedStatement.setString(3, user.getDateOfBirth());
        preparedStatement.setString(4, user.getTelephone());
        preparedStatement.setString(5, user.getEmail());
        preparedStatement.setString(6, user.getBooksBorrowed().toString());
        preparedStatement.executeUpdate();
    }

    public List<String> getBooksBorrowedByTelephone(String telephone) throws SQLException {
        PreparedStatement preparedStatement = this.connection.prepareStatement("select books_borrowed from users where telephone like ?");
        preparedStatement.setString(1, telephone);
        ResultSet resultSet = preparedStatement.executeQuery();

        String result;
        for(result = ""; resultSet.next(); result = result.replaceAll("\\[|]", "")) {
            result = resultSet.getString("books_borrowed");
        }

        return new ArrayList<>(Arrays.asList(result.split("\\s*,\\s*")));
    }
}