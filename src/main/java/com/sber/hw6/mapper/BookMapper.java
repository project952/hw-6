package com.sber.hw6.mapper;

import com.sber.hw6.model.Book;
import java.sql.ResultSet;
import java.sql.SQLException;

public class BookMapper {
    public BookMapper() {
    }

    public Book createBook(ResultSet resultSet) throws SQLException {
        Book book = new Book();
        book.setId(resultSet.getInt("id"));
        book.setTitle(resultSet.getString("title"));
        book.setAuthor(resultSet.getString("author"));
        book.setDateAdded(resultSet.getTimestamp("date_added"));
        return book;
    }
}