package com.sber.hw6;

import com.sber.hw6.dao.BookDao;
import com.sber.hw6.dao.UserDao;
import com.sber.hw6.model.Book;
import com.sber.hw6.model.User;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class Hw6Application implements CommandLineRunner {
    private final BookDao bookDao;
    private final UserDao userDao;

    public Hw6Application(BookDao bookDao, UserDao userDao) {
        this.bookDao = bookDao;
        this.userDao = userDao;
    }

    public static void main(String[] args) {
        SpringApplication.run(Hw6Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        Book book1 = bookDao.findById(1);
        Book book2 = bookDao.findById(2);
        Book book3 = bookDao.findById(3);
        Book book4 = bookDao.findById(4);
        Book book5 = bookDao.findById(5);
        User user1 = new User(
                1,
                "Lundina",
                "Anastasia",
                "10.10.2000",
                "89647474744",
                "email@email.ru",
                new ArrayList<>(List.of(book1.getTitle(), book2.getTitle())));
        User user2 = new User(
                2,
                "Ivanov",
                "Alexander",
                "01.12.1990",
                "+75943839450",
                "mail@mail.ru",
                new ArrayList<>(List.of(book3.getTitle(), book4.getTitle())));
        User user3 = new User(
                3,
                "Petrov",
                "Pavel",
                "21.02.1999",
                "9564638995",
                "maily@maily.ru",
                new ArrayList<>(List.of(book5.getTitle())));
        userDao.addUser(user1);
        userDao.addUser(user2);
        userDao.addUser(user3);
        List<String> result = userDao.getBooksBorrowedByTelephone("89647474744");
        for (String str : result) {
            System.out.println(bookDao.findByTitle(str));
        }
    }
}