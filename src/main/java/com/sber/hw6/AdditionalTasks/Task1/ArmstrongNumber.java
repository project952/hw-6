package com.sber.hw6.AdditionalTasks.Task1;

import java.util.Arrays;
import java.util.List;

public class ArmstrongNumber {
    public static void main(String[] args) {
        List<String> testValues = Arrays.asList("153","154","371","89","1741725");

        for (String value: testValues) {
            System.out.println("Is " + value + " an Armstrong number? -> " + isArmstrongNumber(value));
        }
    }

    public static boolean isArmstrongNumber(String number) {
        int sum = 0;
        char[] numberInChars = number.toCharArray();

        for (char numberInChar : numberInChars) {
            sum += Math.pow(Character.getNumericValue(numberInChar), numberInChars.length);
        }

        return sum == Integer.parseInt(number);
    }
}
