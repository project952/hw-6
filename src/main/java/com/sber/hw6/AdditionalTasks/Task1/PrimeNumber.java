package com.sber.hw6.AdditionalTasks.Task1;

import java.util.Arrays;
import java.util.List;

public class PrimeNumber {
    public static void main(String[] args) {
        List<Integer> testValues = Arrays.asList(1, 2, 3, 4, 7, 99, 5, 17, 24678);

        for (Integer value : testValues) {
            System.out.println("Is " + value + " a Prime number? -> " + isPrimeNumber(value));
        }
    }

    public static boolean isPrimeNumber(Integer number) {
        for (int i = 2; i < number; i++) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }
}
