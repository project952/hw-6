package com.sber.hw6.model;

import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class User {
    private Integer id;
    private String surname;
    private String firstName;
    private String dateOfBirth;
    private String telephone;
    private String email;
    private List<String> booksBorrowed;
}