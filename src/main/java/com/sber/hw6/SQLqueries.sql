create table books
(
    id         serial primary key,
    title      varchar(100) not null,
    author     varchar(100) not null,
    date_added timestamp default now()
);

create table users
(
    id             serial primary key,
    surname        varchar(30) not null,
    first_name     varchar(30) not null,
    date_of_birth  varchar(20) not null,
    telephone      varchar(12) not null,
    email          varchar(50) not null,
    books_borrowed varchar
);

insert into books
values (1, 'Война и мир', 'Л.Н.Толстой', now());
insert into books
values (2, 'Преступление и наказание', 'Ф.М.Достоевский', '2022-10-01');
insert into books
values (3, 'Сказки', 'А.С.Пушкин', '2022-10-05');
insert into books
values (4, 'Три сестры', 'А.П.Чехов', '2022-10-13');
insert into books
values (5, 'На дне', 'М.Горький', '2022-10-01');

select * from books;
select * from users;
